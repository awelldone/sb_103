package dev.siample.sb_103;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Sb103Application {

	public static void main(String[] args) {
		ApplicationContext aContext = SpringApplication.run(Sb103Application.class, args);
		
		String[] beanArray = aContext.getBeanDefinitionNames();
		Arrays.sort(beanArray);
		
		for (String beanName: beanArray) {
			System.out.println(beanName);
		}
	}

}
