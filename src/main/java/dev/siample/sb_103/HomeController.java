package dev.siample.sb_103;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
	
	@Autowired
	SpyGirl spicy;
	
	@RequestMapping("/")
	public String index(){
		return spicy.iSaySg();
	}

}
